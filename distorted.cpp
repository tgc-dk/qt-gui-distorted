#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>
#include <QtWidgets/QFrame>
#include <QtWidgets/QDockWidget>
#include <QtWebEngineWidgets/QWebEngineView>

int main( int argc, char **argv ) {
    QApplication::setAttribute(Qt::AA_ShareOpenGLContexts, true);

    QApplication application(argc, argv);
    // This seems to be the causing the issues
    application.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings, true);

    QMainWindow* main_window = new QMainWindow();
    QWidget* main_content = new QWidget();
    main_window->setCentralWidget(main_content);

    QDockWidget* media_manager_dock = new QDockWidget();
    main_window->addDockWidget(Qt::LeftDockWidgetArea, media_manager_dock);

    QWebEngineView* webview = new QWebEngineView(main_content);
    QFrame* frame = new QFrame(main_content);
    int winid = frame->winId();

    main_window->show();

    return application.exec();
}
